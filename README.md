## Ngx-Animated-Counter

[![Greenkeeper badge](https://badges.greenkeeper.io/vanishdark/angularanimatedcounter.svg)](https://greenkeeper.io/)

![npm](https://img.shields.io/npm/v/ngx-animated-counter)
![Travis (.com) branch](https://img.shields.io/travis/com/vanishdark/angularanimatedcounter/release)
![GitHub issues](https://img.shields.io/github/issues/vanishdark/angularanimatedcounter)
![NPM](https://img.shields.io/npm/l/ngx-animated-counter)

Ngx-Animated-Counter is a simple counter styled with custom themes from bootstrap.
## Table of Contents
[Compatibility](#compatibility)

[Dependencies](#dependencies)

[Installation](#installation)

[Issues](#issues)

[Documentation](#documentation)

### Compatibility

```
Angular	- 7.x.x - 8.x.x
```
#### Dependencies

May need this dependencies to work properly:

```
Bootstrap CSS - 4.x.x < 4.3.1
```

#### Installation
You will need to install this modules in any order, but try to keep this version. If you use other versions and still work let me know.

Install Bootstrap version 4.3.1 (tested).
 ```npm
npm i --save bootstrap@4.3.1
```
Install JQuery required by bootstrap.
```
npm i --save jquery@3.4.1
```
Install Popper.js required by bootstrap.
```
npm i --save popper.js@1.15.0
```
Finally install NgxAnimatedCounter.
 ```npm
npm i --save ngx-animated-counter 
```

#### Issues

If you find any problem, question or suggestion you apply your issue in
[NgxAnimatedCounter Issues](https://github.com/vanishdark/angularanimatedcounter/issues)


#### Documentation

You can find help for most common settings and setup in [NgxAnimatedCounter wiki](https://github.com/vanishdark/angularanimatedcounter/wiki).
